# ArabicNormalizer

Remove unwanted Arabic diacritics, like double Fatha, Kasra or Damma, from your texts with the help of this Java-based application.

This is an old project that is not maintained anymore.

Feel free to fork it and do whatever you want with it.

It was developped with the NetBeans IDE

Under [MIT License](LICENSE.md)
